# hi-gencode

#### 项目介绍
hi-gencode，一款基于springboot，mybatis的代码生成器(目前仅支持mysql)。

具有多表关联生成功能，能够下载生成后的模板文件zip包，默认生成controller、service、entity、mapper、mapperxml文件。

内置有ParamsData（集成查询条件、分页条件、排序条件）类。

该代码生成器运行在浏览器web端，建议使用谷歌浏览器。

演示地址(可能有点卡，毕竟阿里云入门级服务器): http://120.79.5.163:9090/gen/index/to-index

#### 技术选型

1. 后端框架：springboot2.0、freemarker模板引擎、mybatis数据持久层、mysql数据库、druid数据库连接池
2. 前端框架: thymeleaf、vue、axios、element-ui、lodash


#### 代码生成器使用说明（"访问地址：http://localhost:9090/gen/index/to-index"）

![输入图片说明](https://images.gitee.com/uploads/images/2018/0901/214758_ee042120_621372.png "微信图片_20180901214653.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0901/214809_fb2a17ba_621372.png "微信图片_20180901214706.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0901/214820_1faaae59_621372.png "微信图片_20180901214710.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0901/214832_801468a3_621372.png "微信图片_20180901214714.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0901/214844_1e7c6b17_621372.png "微信图片_20180901214716.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0901/214854_e3a7426f_621372.png "微信图片_20180901214719.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0901/214904_c7c6472f_621372.png "微信图片_20180901214722.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0901/214912_f999fc49_621372.png "微信图片_20180901214725.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0901/214920_f72764ed_621372.png "微信图片_20180901214735.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0901/214928_d4bc5451_621372.png "微信图片_20180901214738.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0901/214942_20ab9f7e_621372.png "微信图片_20180901214747.png")



#### 开发说明

1. 需要引入分页maven坐标


```
<dependency>
	<groupId>com.github.pagehelper</groupId>
	<artifactId>pagehelper-spring-boot-starter</artifactId>
	<version>1.2.3</version>
</dependency>
```


2. 代码生成之后需要用到几个关键类，在"开发需要"文件夹中
3. 对应该接口

![输入图片说明](https://images.gitee.com/uploads/images/2018/0901/215448_7cf6a0dc_621372.png "微信图片_20180901215405.png")

前端封装的json格式是

（querys（查询条件）name为对应实体类的字段，opt是查询条件，value是对应查询的值）

（orders（排序条件）name为对应实体类的字段，type为排序条件）

```
{
	"querys": [{
			"name": "orderType",
			"opt": "=",
			"value": "1"
		}, {
			"name": "customerName",
			"opt": "like",
			"value": "liuy"
		}, {
			"name": "createTime",
			"opt": ">=",
			"value": "2018-08-29 00:00:00"
		}, {
			"name": "createTime",
			"opt": "<=",
			"value": "2018-09-04 00:00:00"
		},
		{
			"name": "orderNo",
			"opt": "in",
			"value": ["FD123156FS5GSD", "20180823040809"]
		},
		{
			"name": "orderStatus",
			"opt": "in",
			"value": [1, 2]
		},
		{
			"name": "orderType",
			"opt": "is null"
		},
		{
			"name": "orderType",
			"opt": "is not null"
		}

	],
	"pageInfo": {
		"pageNum": 1,
		"pageSize": 10
	},
	"orders": [{
		"name": "id",
		"type": "desc"
	}]
}
```

4. 如果需要自己建立查询条件、排序条件则需要这样封装


```
ParamsData<OrderEntity.Query,OrderEntity.Order> paramsData=new ParamsData<OrderEntity.Query,OrderEntity.Order>();
paramsData.addQuerys(OrderInfoEntity.getQuery("orderNo","like","WEREWR"));
paramsData.addQuerys(OrderInfoEntity.getQuery("orderStatus","=",1));
paramsData.addQuerys(OrderInfoEntity.getQuery("createTime",">=","2018-09-01 10:00:00"));
paramsData.addQuerys(OrderInfoEntity.getQuery("orderType","is null",""));
paramsData.addQuerys(OrderInfoEntity.getQuery("orderType","is not null",""));

Long[] ids=new Long[0];
String[] strings=new String[0];
paramsData.addQuerys(OrderInfoEntity.getQuery("orderNo","in",strings));
paramsData.addQuerys(OrderInfoEntity.getQuery("orderStatus","in",ids));

paramsData.addOrders(OrderInfoEntity.getOrder("id","desc"));
```

5. 更新需要更新的字段（模板生成之后mapper的updateByColumn的columns参数封装）


```
List<String> columns=new ArrayList<String>();
columns.add(Base.getFieldName("orderNo",OrderInfoEntity.class));
columns.add(Base.getFieldName("orderType",OrderInfoEntity.class));
```

6. 根据某些条件删除对应记录，模板生成mapper的deleteByParam的paramOrderPage参数封装
(和第四条一样)



