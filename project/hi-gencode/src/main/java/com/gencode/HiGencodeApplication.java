package com.gencode;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.util.ClassUtils;

@SpringBootApplication
@MapperScan({"com.gencode.mapper"})
public class HiGencodeApplication {

	public static void main(String[] args) {
		SpringApplication.run(HiGencodeApplication.class, args);
	}
}
